const fs = require('fs');
const path = require('path');
const moment = require('moment');

const Bucket = require('../models/bucket');

const directory = path.join(path.parse(__dirname).dir, 'resources');

exports.createBucket = (req, res) => {
  const { bucketName } = req.params;
  const { create } = req.query;
  if (create !== undefined) {
    Bucket.findOne({ name: bucketName }).exec((_err, docBucket) => {
      if (docBucket) {
        console.log('Create Bucket: bucket already exists.');
        res.status(400).end();
      } else {
        const timestamp = moment().unix();
        const newBucket = new Bucket({
          created: timestamp,
          modified: timestamp,
          name: bucketName,
          objects: [],
        });
        newBucket.save(err => {
          if (err) {
            console.log(err);
            res.status(400).end();
          } else {
            const folderName = path.join(directory, newBucket._id.toString());
            fs.mkdir(folderName, () => {
              res.status(200).send({
                created: newBucket.created,
                modified: newBucket.modified,
                name: newBucket.name,
              });
            });
          }
        });
      }
    });
  }
};

exports.deleteBucket = (req, res) => {
  const { bucketName } = req.params;
  const remove = req.query.delete;
  if (remove !== undefined) {
    Bucket.findOne({ name: bucketName }, (err, docBucket) => {
      if (err || docBucket == null) {
        console.log('Delete Bucket: bucket does not exists.');
        res.status(400).end();
      } else if (docBucket) {
        const folderName = path.join(directory, docBucket._id.toString());
        fs.rmdir(folderName, err => {
          if (err) {
            console.log(`Delete Bucket: cannot remove bucket: ${err}`);
            res.status(400).send({ err });
          } else {
            Bucket.deleteOne({ name: bucketName }, () => {
              res.status(200).send();
            });
          }
        });
      }
    });
  }
};

exports.listBucket = (req, res) => {
  const { bucketName } = req.params;
  const { list } = req.query;
  if (list !== undefined) {
    Bucket.findOne({ name: bucketName }, (err, docBucket) => {
      if (err || docBucket == null) {
        console.log('List Bucket: bucket does not exists.');
        res.status(400).end();
      } else {
        res.status(200).send({
          created: docBucket.created,
          modified: docBucket.modified,
          name: docBucket.name,
          objects: docBucket.objects,
        });
      }
    });
  }
};
