/* eslint-disable no-shadow */
const fs = require('fs');
const path = require('path');
const moment = require('moment');
const crypto = require('crypto');
const rimraf = require('rimraf');
const MultiStream = require('multistream');
const rangeStream = require('range-stream');

const Bucket = require('../models/bucket');

const fsPromise = fs.promises;
const directory = path.join(path.parse(__dirname).dir, 'resources');

exports.mainFunction = (req, res) => {
  const { bucketName } = req.params;
  const { objectName } = req.params;
  const { create } = req.query;
  const { complete } = req.query;
  Bucket.findOne({ name: bucketName }, async (err, docBucket) => {
    if (err || docBucket == null) {
      console.log('Bucket does not exists.');
      res.status(400).end();
    } else {
      const docObject = await docBucket.objects.find(
        e => e.objectName === objectName
      );
      if (create !== undefined) {
        if (docObject !== undefined) {
          console.log('Create Object: object was created before.');
          res.status(400).end();
        } else {
          docBucket.objects.push({
            objectName,
            ticket: 'incomplete',
            hashes: [],
            metadata: [],
            etag: 0,
            totalLength: 0,
          });
          docBucket.modified = moment().unix();
          const updateBucket = await docBucket.save();
          const objectId = updateBucket.objects.find(
            e => e.objectName === objectName
          )._id;
          const folderName = path.join(
            directory,
            updateBucket._id.toString(),
            objectId._id.toString()
          );
          fsPromise.mkdir(folderName);
          res.status(200).end();
        }
      } else if (complete !== undefined) {
        docObject.ticket = 'complete';
        let md5s = '';
        let length = 0;
        for (const part of docObject.hashes) {
          md5s += part.hash;
          length += 1;
        }
        const md5sHexes = Uint8Array.from(Buffer.from(md5s, 'hex'));
        const md5sHexesHashed = crypto
          .createHash('md5')
          .update(md5sHexes)
          .digest('hex');
        const etag = `${md5sHexesHashed}-${length}`;
        docBucket.modified = moment().unix();
        docObject.etag = etag;
        await docBucket.save();
        res.status(200).send({
          etag,
          length: docObject.totalLength,
          name: objectName,
        });
      }
    }
  });
};

exports.uploadFunction = (req, res) => {
  const { bucketName } = req.params;
  const { objectName } = req.params;
  const { partNumber } = req.query;
  const { metadata } = req.query;
  const md5FromClient = req.headers['content-md5'];
  const lengthFromClient = req.headers['content-length'];
  Bucket.findOne({ name: bucketName }, async (err, docBucket) => {
    const docObject = await docBucket.objects.find(
      e => e.objectName === objectName
    );
    if (partNumber) {
      const hash = crypto
        .createHash('md5')
        .update(req.body)
        .digest('hex');
      if (err || docBucket == null) {
        res.status(400).send({
          md5: md5FromClient,
          length: lengthFromClient,
          partNumber,
          error: 'bucket does not exists.',
        });
      } else if (docObject === undefined) {
        res.status(400).send({
          md5: md5FromClient,
          length: lengthFromClient,
          partNumber,
          error: 'object was not created before.',
        });
      } else if (hash !== md5FromClient.toLowerCase()) {
        res.status(400).send({
          md5: md5FromClient,
          length: lengthFromClient,
          partNumber,
          error: 'md5 does not match.',
        });
      } else if (req.body.length !== Number(lengthFromClient)) {
        res.status(400).send({
          md5: md5FromClient,
          length: lengthFromClient,
          partNumber,
          error: 'length does not match.',
        });
      } else if (partNumber < 1 || partNumber > 10000) {
        res.status(400).send({
          md5: md5FromClient,
          length: lengthFromClient,
          partNumber,
          error: 'invalid part number.',
        });
      } else if (docObject.ticket === 'complete') {
        res.status(400).send({
          md5: md5FromClient,
          length: lengthFromClient,
          partNumber,
          error: 'object marked as complete.',
        });
      } else {
        const isExist = docObject.hashes.find(e => e.part === partNumber);
        if (isExist) {
          res.status(400).send({
            md5: md5FromClient,
            length: lengthFromClient,
            partNumber,
            error: 'part already uploaded.',
          });
        } else {
          docObject.hashes.push({ part: partNumber, hash: md5FromClient });
          docObject.totalLength += lengthFromClient;
          docBucket.modified = moment().unix();
          await docBucket.save();
          fsPromise.writeFile(
            path.join(
              directory,
              docBucket._id.toString(),
              docObject._id.toString(),
              md5FromClient
            ),
            req.body
          );
          res.status(200).send({
            md5: md5FromClient,
            length: lengthFromClient,
            partNumber,
          });
        }
      }
    } else if (metadata !== undefined) {
      if (err || docBucket == null) {
        console.log('Bucket does not exists.');
        res.status(404).end();
      } else if (docObject === undefined) {
        console.log('Object does not exists.');
        res.status(404).end();
      } else {
        const { key } = req.query;
        const value = req.body.toString();
        const isExist = docObject.metadatas.find(e => e.key === key);
        if (isExist) {
          docObject.metadatas = await docObject.metadatas.filter(
            e => e !== isExist
          );
        }
        await docObject.metadatas.push({
          key,
          value,
        });
        docBucket.modified = moment().unix();
        await docBucket.save();
        res.status(200).end();
      }
    }
  });
};

exports.deleteFunction = (req, res) => {
  const { bucketName } = req.params;
  const { objectName } = req.params;
  const { partNumber } = req.query;
  const { metadata } = req.query;
  Bucket.findOne({ name: bucketName }, async (err, docBucket) => {
    if (err || docBucket == null) {
      console.log('Bucket does not exists.');
      res.status(400).end();
    } else {
      const docObject = await docBucket.objects.find(
        e => e.objectName === objectName
      );
      if (docObject === undefined) {
        console.log('Object does not exists.');
        res.status(400).end();
      } else if (partNumber) {
        if (docObject.ticket === 'complete') {
          console.log('Delete Part: object has been marked as complete.');
          res.status(400).end();
        } else {
          const isExist = docObject.hashes.find(e => e.part === partNumber);
          if (!isExist) {
            console.log('Delete Part: part does not exists.');
            res.status(400).end();
          } else {
            docObject.hashes = docObject.hashes.filter(e => e !== isExist);
            docBucket.modified = moment().unix();
            await docBucket.save();
            fsPromise.unlink(
              path.join(
                directory,
                docBucket._id.toString(),
                docObject._id.toString(),
                isExist.hash
              )
            );
            res.status(200).end();
          }
        }
      } else if (req.query.delete === '') {
        rimraf(
          path.join(
            directory,
            docBucket._id.toString(),
            docObject._id.toString()
          ),
          () => {
            res.status(200).send();
          }
        );
        docBucket.objects = docBucket.objects.filter(e => e !== docObject);
        docBucket.modified = moment().unix();
        await docBucket.save();
      } else if (metadata !== undefined) {
        const { key } = req.query;
        docObject.metadatas = await docObject.metadatas.filter(
          e => e.key !== key
        );
        docBucket.modified = moment().unix();
        await docBucket.save();
        res.status(200).end();
      }
    }
  });
};

exports.downloadFunction = (req, res) => {
  const { bucketName } = req.params;
  const { objectName } = req.params;
  const { metadata } = req.query;
  Bucket.findOne({ name: bucketName }, async (err, docBucket) => {
    if (err || docBucket == null) {
      console.log('Bucket does not exists.');
      res.status(404).end();
    } else {
      const docObject = await docBucket.objects.find(
        e => e.objectName === objectName
      );
      if (docObject === undefined) {
        console.log('Object does not exists.');
        res.status(404).end();
      } else if (metadata !== undefined) {
        const { key } = req.query;
        const result = {};
        const { metadatas } = docObject;
        if (key === undefined) {
          metadatas.forEach(data => {
            result[data.key] = data.value;
          });
          res.status(200).send(result);
        } else {
          metadatas.forEach(data => {
            if (data.key === key) {
              result[data.key] = data.value;
            }
          });
          res.status(200).send(result);
        }
      } else {
        const streams = [];
        docObject.hashes.forEach(data => {
          streams.push(
            fs.createReadStream(
              path.join(
                directory,
                docBucket._id.toString(),
                docObject._id.toString(),
                data.hash
              )
            )
          );
        });
        if (req.headers.range === undefined) {
          new MultiStream(streams).pipe(res);
        } else {
          const range = req.headers.range.match(/\d+/g);
          new MultiStream(streams)
            .pipe(rangeStream(Number(range[0]), Number(range[1])))
            .pipe(res);
        }
      }
    }
  });
};
