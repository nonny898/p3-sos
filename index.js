const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const routes = require('./routes/index');
const config = require('./config/config');

const app = express();

app.use(
  bodyParser.raw({
    type: '*/*',
    limit: '1024mb',
  })
);

app.use('/', routes);

mongoose.connect(config.DB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

module.exports = app;
