const mongoose = require('mongoose');

const { Schema } = mongoose;

const objectSchema = new Schema({
  objectName: String,
  ticket: String,
  hashes: [],
  metadatas: [],
  etag: String,
  totalLength: Number,
});

const bucketSchema = new Schema({
  created: String,
  modified: String,
  name: String,
  objects: [objectSchema],
});

module.exports = mongoose.model('Bucket', bucketSchema);
