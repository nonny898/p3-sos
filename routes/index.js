const express = require('express');

const router = express.Router();
const bucketController = require('../controllers/bucket');
const objectController = require('../controllers/object');

// For buckets

router.post('/:bucketName', bucketController.createBucket);

router.delete('/:bucketName', bucketController.deleteBucket);

router.get('/:bucketName', bucketController.listBucket);

// For objects and metadata

router.post('/:bucketName/:objectName', objectController.mainFunction);

router.put('/:bucketName/:objectName', objectController.uploadFunction);

router.delete('/:bucketName/:objectName', objectController.deleteFunction);

router.get('/:bucketName/:objectName', objectController.downloadFunction);

module.exports = router;
