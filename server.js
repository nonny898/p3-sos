const app = require('./index');
const config = require('./config/config');

app.listen(config.APP_PORT);
