const supertest = require('supertest');
const app = require('../index');

const request = supertest(app);

describe('POST /:bucketName', () => {
  afterEach(async () => {
    await request.delete('/createBucket').query('delete');
  });
  it('create new bucket', async () => {
    const response = await request.post('/createBucket').query('create');
    expect(response.status).toBe(200);
    expect(response.body.created).toBeDefined();
    expect(response.body.modified).toBeDefined();
    expect(response.body.name).toBe('createBucket');
  });
  it('recreate bucket', async () => {
    await request.post('/createBucket').query('create');
    const response = await request.post('/createBucket').query('create');
    expect(response.status).toBe(400);
  });
});

describe('GET /:bucketName', () => {
  beforeEach(async () => {
    await request.post('/createBucket').query('create');
  });
  afterEach(async () => {
    await request.delete('/createBucket').query('delete');
  });
  it('list bucket', async () => {
    const response = await request.get('/createBucket').query('list');
    expect(response.status).toBe(200);
    expect(response.body.created).toBeDefined();
    expect(response.body.modified).toBeDefined();
    expect(response.body.name).toBe('createBucket');
    expect(response.body.objects).toBeDefined();
  });
  it('list non-bucket', async () => {
    const response = await request.get('/otherBucket').query('list');
    expect(response.status).toBe(400);
  });
});

describe('DELETE /:bucketName', () => {
  it('delete bucket', async () => {
    await request.post('/createBucket').query('create');
    const response = await request.delete('/createBucket').query('delete');
    expect(response.status).toBe(200);
  });
  it('delete non exist bucket', async () => {
    const response = await request.delete('/otherBucket').query('delete');
    expect(response.status).toBe(400);
  });
});
