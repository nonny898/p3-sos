const supertest = require('supertest');
const fs = require('fs');
const path = require('path');
const crypto = require('crypto');
const app = require('../index');

const request = supertest(app);

const testfile = path.join(path.parse(__dirname).dir, 'test', 'readfile.txt');

describe('POST /:bucketName/:objectName', () => {
  it('create new object', async done => {
    // await request.post('/createBucket').query('create');
    // const response = await request
    //   .post('/createBucket/createObject')
    //   .query('create');
    // expect(response.status).toBe(200);
    done();
  });
  it('recreate object', async done => {
    // const response = await request
    //   .post('/createBucket/createObject')
    //   .query('create');
    // expect(response.status).toBe(400);
    done();
  });
});

describe('PUT /:bucketName/:objectName', () => {
  it('should upload all parts', async done => {
    // const readFile = fs.readFileSync(testfile);
    // const hash = crypto
    //   .createHash('md5')
    //   .update(readFile)
    //   .digest('hex');
    // const req = request
    //   .put('/createBucket/createObject')
    //   .query({
    //     partNumber: 1,
    //   })
    //   .set('content-md5', hash)
    //   .set('content-length', readFile.length);
    // req.type('application/octet-stream');
    // req.write(readFile);
    // const response = await req;
    // expect(response.status).toBe(200);
    // expect(response.body.length).toBe(String(readFile.length));
    // expect(response.body.partNumber).toBe(String(1));
    done();
  });
  it('should mark the upload as completed', async done => {
    // const response = await request
    //   .post('/createBucket/createObject')
    //   .query('complete');
    // expect(response.statusCode).toBe(200);
    // expect(response.body.etag).toBeDefined();
    // expect(response.body.length).toBeDefined();
    // expect(response.body.name).toBeDefined();
    done();
  });
  it('should add/update metadata by key', async () => {
    // const response = await request
    //   .put('/createBucket/createObject')
    //   .query('metadata')
    //   .query({
    //     key: 'license',
    //   })
    //   .send('Apache 2.0');
    // expect(response.statusCode).toBe(200);
  });
  it('should get metadata by key', async () => {
    // const response = await request
    //   .get('/createBucket/createObject')
    //   .query('metadata')
    //   .query({
    //     key: 'license',
    //   });
    // expect(response.statusCode).toBe(200);
    // expect(response.body.license).toBe('Apache 2.0');
  });
  it('should get all metadata', async () => {
    // await request
    //   .put('/createBucket/createObject')
    //   .query('metadata')
    //   .query({
    //     key: 'dataSource',
    //   })
    //   .send('http://www.ietf.org/rfc/rfc2616.txt');
    // const response = await request
    //   .get('/createBucket/createObject')
    //   .query('metadata');
    // expect(response.statusCode).toBe(200);
    // expect(response.body.license).toBe('Apache 2.0');
    // expect(response.body.dataSource).toBe(
    //   'http://www.ietf.org/rfc/rfc2616.txt'
    // );
  });
});

describe('DELETE /:bucketName/:objectName', () => {
  it('can not delete part if completed', async () => {
    // const response = await request.delete('/createBucket/createObject').query({
    //   partNumber: 1,
    // });
    // expect(response.statusCode).toBe(400);
  });
  it('should remove metadata by key', async () => {
    // const response = await request
    //   .delete('/createBucket/createObject')
    //   .query('metadata')
    //   .query({
    //     key: 'license',
    //   });
    // expect(response.statusCode).toBe(200);
  });
  it('can delete object', async () => {
    // const response = await request
    //   .delete('/createBucket/createObject')
    //   .query('delete');
    // expect(response.statusCode).toBe(200);
  });
});
